# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import Response,request
import json
import werkzeug.utils

class Session(http.Controller):

	@http.route('/getListeDevisByState/valide', type='http', auth="none")
	def getListeDevisByState(self, base_location=None):
	    request.session.authenticate("suivie_projet", "admin", "admin")
	    value = []
	    note_frais_all = request.env["notaire.devis"].search([(1, '=', 1)])
	    for item in note_frais_all:
		note_frais = request.env["notaire.devis"].browse(item.id)
		nature_operation = "" 
		if note_frais.nature_operation == "promess_vente" :
			nature_operation = "Promesse de vente"
		elif note_frais.nature_operation == "acquisition" :
			nature_operation = "Acquisition"
		elif note_frais.nature_operation == "donation" :
			nature_operation = "Donation"
		elif note_frais.nature_operation == "constitution_hypotheque" :
			nature_operation = "Constitution d'hypothèque"
		elif note_frais.nature_operation == "mainleve_hypotheque" :
			nature_operation = "Mainlevée d'hypothèque"
		dict_item = {
			"clientName": note_frais.client.name,
			"natureOperation": nature_operation,#note_frais.nature_operation,
			"dateDevis" : note_frais.date_creation,
			"montant": note_frais.montant_total,
			"status" : note_frais.state,
			"id":note_frais.id,
		}
		value.append(dict_item)
	    print value
	    return json.dumps(value)



	@http.route('/getInfoDevis/<int:id_devis>', type='http', auth="none")
	def getInfoDevis(self, id_devis, base_location=None):
	    request.session.authenticate("suivie_projet", "admin", "admin")
	    value = []
	    request.session.authenticate("suivie_projet", "admin", "admin")
	    note_frais = request.env["notaire.devis"].browse(id_devis)
	    dict_item = {
			"clientName": note_frais.client.name,
			"natureOperation": note_frais.nature_operation,
			"Telephone" : note_frais.client.phone,
			"montant": note_frais.montant_total,
			"status" : note_frais.state,
			"enregistrement": note_frais.frais_enregistrement,
			"fraisCf":note_frais.frais_cf,
			"fraisHypotheque": note_frais.frais_cf_hypotheque,
			"certificat":note_frais.frais_certificat,
			"honoraire": note_frais.frais_honoraire,
			"tvaHonoraier": note_frais.frais_tva_honoraire,
			"droitActe":note_frais.frais_droit_acte,
			"autreFormalite": 0,
		    }  
	    value.append(dict_item)
	    print value
	    return json.dumps(value)


	@http.route('/getListeDossierByState/<string:state>', type='http', auth="none")
	def getListeDossierByState(self, state, base_location=None):
	    request.session.authenticate("suivie_projet", "admin", "admin")
	    value = []
	    print state
	    dossier_all = request.env["notaire.suivie"].search([('state', '=', state)])
	    print dossier_all
	    for item in dossier_all:
	    	dossier = request.env["notaire.suivie"].browse(item.id)
	    	dict_item = {
			"clientName": dossier.client.name,
			"natureOperation": dossier.nature_operation,
			"dateDossier" : dossier.date_dossier,
			"repertoireNum" : dossier.num_dossier,
			"tf": dossier.num_tf,
			"status" : dossier.state,
			"id":dossier.id,
		    }
	    	value.append(dict_item)
	    print value
	    return json.dumps(value)


	@http.route('/getInfoDossier/<int:id_dossier>', type='http', auth="none")
	def getInfoDossier(self, id_dossier, base_location=None):
	    request.session.authenticate("suivie_projet", "admin", "admin")
	    value = []
    	    dossier = request.env["notaire.suivie"].browse(id_dossier)
	    dict_item = {
		    "clientName": dossier.client.name,
		    "natureOperation": dossier.nature_operation,
		    "repertoireNum" : dossier.num_dossier,
		    "tf": dossier.num_tf,
		    "status" : dossier.state,
		    "formalite_dgi" : {
			    "date_paiement":dossier.dgi_date_paiement,
			    "montant": dossier.dgi_montant_paye,
		    	},
		    "formalite_cf" : {
			    "dateDepot":dossier.ancfcc_date_depot,
			    "dateValidation":dossier.ancfcc_date_validation,
			    "dateDepotPhysique":dossier.ancfcc_date_depot_physique,
			    "datePaiement":dossier.ancfcc_date_paiement,
			    "montant": dossier.ancfcc_montant_paye,
		   	},
		    "formalite_bnq" : {
			"banque":dossier.bnq_banque,
			"contact":dossier.bnq_contacte,
			"dateEnvoiEngagement":dossier.bnq_date_engagement,
			"dateEnvoiActe":dossier.bnq_date_engagement,
			"dateValidation":dossier.bnq_date_validation,
			"dateReceptionCheque":dossier.bnq_date_deblocage,
			"dateSignature":dossier.bnq_date_signature,
			"montant": 200000
		    	},
		    "formalite_tgr" : {
			#"dateEnvoi":"12-05-2020",
			#"dateValidation":"12-05-2020"
		    	},
		    "formalite_tva" : {
			"dateEnvoi":dossier.tva_date_demande,
			"dateValidation":dossier.tva_date_validation,
			"dateReception":dossier.tva_date_virement_cdg,
			"montant": dossier.tva_montant,
		    	},
		    }
    	    value.append(dict_item)
	    print value
	    return json.dumps(value)




