 openerp.contentieux = function (openerp)
{  
//=====================================================================================================================================
openerp.web.form.widgets.add('statistique_categories', 'openerp.contentieux.categories');
    openerp.contentieux.categories = openerp.web.form.FieldChar.extend({
        template : "statistique_categories",
//=======
        init: function (view, code) {
		this._super(view, code);
		var ds = new openerp.web.DataSet(this, 'contentieux.statistiques', {});

		prepartYearSelect(ds);
		prepartAvocatsSelect(ds);
		prepartCategorieSelect(ds);
		
		
		prepartNatureLitigeSelect(ds);

prepartDataAndMenu(ds,"categorie_graph","Affaire par avocat","avocat",'pie',$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"state_graph","Statut de l'affaire","state",'doughnut',$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"etat_graph","Affaire par département","department",'line',$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"nature_graph","Affaire par nature du litige","nature_litige",'column',$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
        },// fiiiiiiiiiiiiiiiiiiiiiiiin init
//======
	start: function() {
		var ds = new openerp.web.DataSet(this, 'contentieux.statistiques', {});
	// ***************************************
	$(".modeGraph").on('click', function () {
	$("#modeGraph").val($(this).attr('valeur'));
prepartDataAndMenu(ds,"categorie_graph","Affaire par avocat","avocat",$("#typeGraphCategorie").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"state_graph","Statut de l'affaire","state",$("#typeGraphState").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"etat_graph","Affaire par département","department",$("#typeGraphEtat").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"nature_graph","Affaire par nature du litige","nature_litige",$("#typeGraphNature").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
			$(".modeGraph").removeClass("button_titre_graph_focus");
			$(this).addClass("button_titre_graph_focus");
	});
	// ***************************************
	$("#yearValue, #avocatValue, #categorieValue, #statutValue, #etatValue, #natureValue, #typePartieValue, #cgi_partie").change('click', function () {
prepartDataAndMenu(ds,"categorie_graph","Affaire par avocat","avocat",$("#typeGraphCategorie").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"state_graph","Statut de l'affaire","state",$("#typeGraphState").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"etat_graph","Affaire par département","department",$("#typeGraphEtat").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
prepartDataAndMenu(ds,"nature_graph","Affaire par nature du litige","nature_litige",$("#typeGraphNature").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
	});
	// ***************************************
	$("#secteurCategorie, #anneauCategorie, #corbeCategorie, #histogrammeCategorie, #tableauCategorie").on('click', function () {
		//$("#typeGraphCategorie").html($(this).html());
		$("#typeGraphCategorie").attr({'valeur':$(this).attr('valeur')});
		prepartDataAndMenu(ds,"categorie_graph","Affaire par avocat","avocat",$("#typeGraphCategorie").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
	});	
	// ***************************************
	$("#secteurState, #anneauState, #corbeState, #histogrammeState, #tableauState").on('click', function () {
		//$("#typeGraphState").html($(this).html());
		$("#typeGraphState").attr({'valeur':$(this).attr('valeur')});
		prepartDataAndMenu(ds,"state_graph","Statut de l'affaire","state",$("#typeGraphState").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
	});	
	// ***************************************
	$("#secteurEtat, #anneauEtat, #corbeEtat, #histogrammeEtat, #tableauEtat").on('click', function () {
		//$("#typeGraphEtat").html($(this).html());
		$("#typeGraphEtat").attr({'valeur':$(this).attr('valeur')});
		prepartDataAndMenu(ds,"etat_graph","Affaire par département","department",$("#typeGraphEtat").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
	});	
	// ***************************************
	$("#secteurNature, #anneauNature, #corbeNature, #histogrammeNature, #tableauNature").on('click', function () {
		//$("#typeGraphNature").html($(this).html());
		$("#typeGraphNature").attr({'valeur':$(this).attr('valeur')});
		prepartDataAndMenu(ds,"nature_graph","Affaire par nature du litige","nature_litige",$("#typeGraphNature").attr('valeur'),$("#modeGraph").val(),$('#yearValue').val(),$('#avocatValue').val(),$('#categorieValue').val(),$('#statutValue').val(),$('#etatValue').val(),$('#natureValue').val(),$("#typePartieValue").val(),$("#cgi_partie").val());
	});
	
	// ***************************************
	},// fiiiiiiiiiiiiiiiiiiiiiiiiin start
//======
    });//openerp.notaire_statistique.categories

//=====================================================================================================================================

openerp.web.form.widgets.add('calendar_echeances', 'openerp.contentieux.calendar');
    openerp.contentieux.calendar = openerp.web.form.FieldChar.extend({
        template : "calendar_echeances",
//=======
        init: function (view, code) {
		this._super(view, code);
		$(".oe_view_manager_header").remove();
		
		
        },// fiiiiiiiiiiiiiiiiiiiiiiiin init
//======
	start: function() {
		//window.location.href
		url = "https://www.contencia-soft.com"
		var ds = new openerp.web.DataSet(this, 'contentieux.correspondance', {});
		getCalendar(ds,url,true,true,true);
// --------------------------------------------------
		$("#testdestroy").click(function() {
			$('#calendarEcheances').fullCalendar('destroy');
				ds.call('getEcheances', [url,true,true,true]).done(function(data) {
					$('#calendarEcheances').fullCalendar({
						locale: 'fr',
						editable: false,
						eventLimit: true, // allow "more" link when too many events
						firstDay: 1,
						columnFormat : 'dddd',
						monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
						monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
						dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
						dayNamesShort: ['Dim.','Lun.','Mar.','Mer.','Jeu.','Ven.','Sam.'],
						events: data,
						header: {
							left: 'prev,next today',
							center: 'title',
							right: '' //'month,agendaWeek,agendaDay'
						    }
					});
					$('.fc-button-today').html("Aujourd'hui");
				})
		});
// --------------------------------------------------
		$("#spaneventcalendar").click(function() {
			$('#calendarEcheances').fullCalendar('destroy');
			if($("#eventcalendar").is(':checked')){
				$("#eventcalendar").attr('checked', false);
				getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}else{
				$("#eventcalendar").attr('checked', true);
				getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}
		});

		$("#eventcalendar").click(function() {
			$('#calendarEcheances').fullCalendar('destroy');
			if($("#eventcalendar").is(':checked')){
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}else{
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}
		});




		$("#spancorrespondancecalendar").click(function() {
			$('#calendarEcheances').fullCalendar('destroy');
			if($("#correspondancecalendar").is(':checked')){
				$("#correspondancecalendar").attr('checked', false);
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}else{
				$("#correspondancecalendar").attr('checked', true);
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}
		});



		$("#correspondancecalendar").click(function() {
			$('#calendarEcheances').fullCalendar('destroy');
			if($("#correspondancecalendar").is(':checked')){
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}else{
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}
		});



		$("#spanfacturecalandar").click(function() {
			$('#calendarEcheances').fullCalendar('destroy');
			if($("#facturecalandar").is(':checked')){
				$("#facturecalandar").attr('checked', false);
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}else{
				$("#facturecalandar").attr('checked', true);
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}
		});

		$("#facturecalandar").click(function() {
			$('#calendarEcheances').fullCalendar('destroy');
			if($("#facturecalandar").is(':checked')){
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}else{
	getCalendar(ds,url,$("#eventcalendar").is(':checked'),$("#correspondancecalendar").is(':checked'),$("#facturecalandar").is(':checked'));
			}
		});







	},// fiiiiiiiiiiiiiiiiiiiiiiiiin start
//======
    });//openerp.notaire_statistique.state


//=====================================================================================================================================

openerp.web.form.widgets.add('statistique_evaluation', 'openerp.contentieux.evaluation');
    openerp.contentieux.evaluation = openerp.web.form.FieldChar.extend({
        template : "statistique_evaluation",
//=======
        init: function (view, code) {
		this._super(view, code);
		$(".oe_view_manager_header").remove();
		var ds = new openerp.web.DataSet(this, 'contentieux.statistiques', {});
		prepartYearSelect(ds);
		prepartSelectCretaire(ds,"state","#statutValue");
		
		getEvaluation(ds,$('#statutValue').val(),$('#etatValue').val(),$('#yearValue').val());
		getEvaluationTableau(ds,$('#statutValue').val(),$('#etatValue').val(),$('#yearValue').val());
		getBillan(ds,$('#yearValue').val());
		getBillanTableau(ds,$('#yearValue').val());
        },// fiiiiiiiiiiiiiiiiiiiiiiiin init
//======
	start: function() {
		
		var ds = new openerp.web.DataSet(this, 'contentieux.statistiques', {});
		
		$("#yearValue").change('click', function () {
			getEvaluation(ds,$('#statutValue').val(),$('#etatValue').val(),$('#yearValue').val());
			getEvaluationTableau(ds,$('#statutValue').val(),$('#etatValue').val(),$('#yearValue').val());
			getBillan(ds,$('#yearValue').val());
			getBillanTableau(ds,$('#yearValue').val());
		});	
		$("#avocatValue, #categorieValue, #statutValue, #etatValue, #natureValue").change('click', function () {
			getEvaluation(ds,$('#statutValue').val(),$('#etatValue').val(),$('#yearValue').val());
			getEvaluationTableau(ds,$('#statutValue').val(),$('#etatValue').val(),$('#yearValue').val());
		});	
		
	},// fiiiiiiiiiiiiiiiiiiiiiiiiin start
//======
    });//openerp.


//=====================================================================================================================================

openerp.web.form.widgets.add('import_affaire', 'openerp.contentieux.affaire');
    openerp.contentieux.affaire = openerp.web.form.FieldChar.extend({
        template : "import_affaire",
//=======
        init: function (view, code) {
		this._super(view, code);
		$(".oe_view_manager_header").remove();
		var dsUser = new openerp.web.DataSet(this, 'contentieux.affaire', {});
		prepartUsersInSelect(dsUser)
        },// fiiiiiiiiiiiiiiiiiiiiiiiin init
//======
	start: function() {
		
		//var ds = new openerp.web.DataSet(this, 'contentieux.statistiques', {});
		
	$(".msgok").hide();
	$(".msgoff").hide();

	



var fileInput = document.getElementById("csv");
var html = "";
    readFile = function () {
        var reader = new FileReader();
        reader.onload = function () {
		resultat = reader.result.split("\n");
		html = "<table class='tableStatistique' style='width: 100%;'><thead><tr><td>Statut de l'affaire </td><td>Référence Pré-Contentieux</td><td>Référence Contentieux</td><td>Type partie </td><td>Demandeur Type</td><td>Demandeur Nom</td><td>Demandeur Prénom</td><td>Demandeur Dénomination</td><td>Défendeur Type</td><td>Défendeur Nom</td><td>Défendeur Prénom</td><td>Défendeur Dénomination</td><td>Avocat Nom/Prénom</td><td>Projet Dénomination</td><td>Département Dénomination</td><td>Catégorie libelle</td><td>Nature de litige libelle</td><td>Date pré-contentieux</td><td>Date contentieux</td><td>Montant du litige</td><td>Evaluation du risque</td><td>Provision</td><td>Prévision honoraire</td><td>Clôturé le</td><td>Type d'évenement</td><td>Juridiction</td><td>Instance</td><td>Jugement Final</td><td>N° Dossier</td><td>Date</td><td>Etat de l'affaire</td><td>Montant G/P</td><td>Date de l'écheance</td></tr></thead><tbody id='allDataImport'>"

		for(var i=0;i<resultat.length-1;i++){
			tdResult = resultat[i].split(";");
			//<td><input type='checkbox' class='singelCheck' checked='true' onClick='this.checked='/></td>
			html += "<tr>";
			for(var j=0;j<tdResult.length;j++){
				html += "<td>"+tdResult[j].decode()+"</td>"
			}
			html += "</tr>";
		}
		html += "</tbody></table>"
        	$("#myOut").html(html);
		$("#myOut").show();
		$(".beforImport").hide();
		$(".valideDIV").show();
        };
        reader.readAsBinaryString(fileInput.files[0]);

    };
fileInput.addEventListener('change', readFile);


$('#showInfo').click(function(){ 
	//$('.infoImport').toggle();
	if ($('.infoImport').css('display') == 'none'){
		$('.infoImport').show('slow');
		$('#showInfo').val("Cacher l'exemple");
	}else{
		$('.infoImport').hide('slow');$('#showInfo');
		$('#showInfo').val('Affichier un exemple');
	}
});

$('#OpenUpload').click(function(){ 
	$('#csv').trigger('click'); 
});

 $('#valideCSV').on('click', function () {
	var datas = []
	$('#allDataImport tr').each(function (i, el) {
		var $tds = $(this).find('td');
		 vals = {
			"state":$tds.eq(0).text(),
			"ref_precontentieux":$tds.eq(1).text(),
			"ref_contentieux":$tds.eq(2).text(),
			"typepartie":$tds.eq(3).text(),
			"DemandeurType":$tds.eq(4).text(),
			"DemandeurNom":$tds.eq(5).text(),
			"DemandeurPrenom":$tds.eq(6).text(),
			"DemandeurDenomination":$tds.eq(7).text(),
			"DefendeurType":$tds.eq(8).text(),
			"DefendeurNom":$tds.eq(9).text(),
			"DefendeurPrenom":$tds.eq(10).text(),
			"DefendeurDenomination":$tds.eq(11).text(),
			"AvocatNomPrenom":$tds.eq(12).text(),
			"projet":$tds.eq(13).text(),
			"department":$tds.eq(14).text(),
			"categorie":$tds.eq(15).text(),
			"nature_litige":$tds.eq(16).text(),
			"precontentieux_date":$tds.eq(17).text(),
			"open_date":$tds.eq(18).text(),
			"mantant_litige":$tds.eq(19).text(),
			"evaluation_risque":$tds.eq(20).text(),
			"provision":$tds.eq(21).text(),
			"previsionHonoraire":$tds.eq(22).text(),
			"close_date":$tds.eq(23).text(),
			'type_evenement':$tds.eq(24).text(),
			'juridiction_id':$tds.eq(25).text(),
			'instance_id':$tds.eq(26).text(),
			'jugement_final':$tds.eq(27).text(),
			'num_dossier':$tds.eq(28).text(),
			'date':$tds.eq(29).text(),
			'etat_affaire':$tds.eq(30).text(),
			'montant':$tds.eq(31).text(),
			'date_echeance' :$tds.eq(32).text(),
			}
		datas.push(vals);
	    });

	var ds = new openerp.web.DataSet(this, 'contentieux.affaire', {});
	ds.call('checkImport', [datas,parseInt($("#userAdd").val())]).done(function(data) {
		if (data>0){
			$(".beforImport").hide();
			$(".valideDIV").hide();
			$(".msgok").show();
		}else{
			$(".beforImport").hide();
			$(".valideDIV").hide();
			$(".msgoff").show();
		}
	});
});

	},// fiiiiiiiiiiiiiiiiiiiiiiiiin start
//======
    });// 'import_affaire'




//=====================================================================================================================================

openerp.web.form.widgets.add('accuiel_contentcia', 'openerp.contentieux.statistique');
    openerp.contentieux.statistique = openerp.web.form.FieldChar.extend({
        template : "accuiel_contentcia",
//=======
        init: function (view, code) {
		this._super(view, code);
		$(".oe_view_manager_header").remove();
		
        },// fiiiiiiiiiiiiiiiiiiiiiiiin init
//======
	start: function() {
		

	},// fiiiiiiiiiiiiiiiiiiiiiiiiin start
//======
    });// 'import_affaire'




//§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ fin
}//openerp.web.form.widgets.add('statistique_actes_communique'



function getEvaluation(ds,state,etat,anne){
	ds.call('getProvision', [state,etat,anne,'graph']).done(function(data) {
    if (data){
	myDataP1 = [];
		myDataP2 = [];
		myDataSplit = data.split('<>');
		for(var i=0;i<myDataSplit.length-1;i++){
			column = myDataSplit[i].split('=>');
			myDataP1.push({  label: column[0], y: parseInt(column[2])});
			myDataP2.push({  label: column[0], y: parseInt(column[1])});
		}

		var chart = new CanvasJS.Chart("evaluation_graph", {
			animationEnabled: true,
			exportFileName: "Provision des affaires",
			exportEnabled: true,
			title:{
				text: "Provision des affaires"              
			},
			axisY:{        
				suffix: " DH"
			      },
			data: [
			      {        
				type: "column",
				dataPoints: myDataP1,
				toolTipContent: "{label} : <strong>{y}</strong> DH",
				legendText: "Provision",
				showInLegend: true,
			      },
			      {        
				type: "area",
				dataPoints: myDataP2,
				toolTipContent: "{label} : <strong>{y}</strong> DH",
				legendText: "Montant du litige",
				showInLegend: true,
			      }
			]
		});
		chart.render();
		$('.canvasjs-chart-credit').css('display','none');				
    }else{
		$("#evaluation_graph").html("<p style='margin: auto;margin-top: 20%;'>Aucun résultat trouvé pour votre requête</br><b>Provision des affaires</b></p>")		
    }
		
		
	})
}



function getBillan(ds,anne){
	ds.call('getSolution', [anne,'graph']).done(function(data) {
    if (data){
		myDataP1 = [];
		myDataP2 = [];
		myDataSplit = data.split('<>');
		for(var i=0;i<myDataSplit.length-1;i++){
			column = myDataSplit[i].split('=>');
			myDataP1.push({  label: column[0], y: parseInt(column[1])});
			myDataP2.push({  label: column[0], y: parseInt(column[2])});
		}

		var chart = new CanvasJS.Chart("solution_graph", {
			animationEnabled: true,
			exportFileName: "Bilan financier",
			exportEnabled: true,
			title:{
				text: "Bilan financier"              
			},
			axisY:{        
				suffix: " DH"
			      },
			data: [
			      {        
				type: "column",
				dataPoints: myDataP1,
				toolTipContent: "{label} : <strong>{y}</strong> DH",
				legendText: "Montant gagné",
				showInLegend: true,
			      },
				{        
				type: "column",
				dataPoints: myDataP2,
				toolTipContent: "{label} : <strong>{y}</strong> DH",
				legendText: "Montant perdu",
				showInLegend: true,
			      }
			]
		});
		chart.render();
		$('.canvasjs-chart-credit').css('display','none');
    }else{
		$("#solution_graph").html("<p style='margin: auto;margin-top: 20%;'>Aucun résultat trouvé pour votre requête</br><b>Bilan financier</b></p>")		
    }
	})
}



function getEvaluationTableau(ds,state,etat,anne){
	ds.call('getProvision', [state,etat,anne,"tableau"]).done(function(data) {
    if (data){
	$("#evaluation_tableau").html(getMyHTMLMulti(data,"Provision des affaires",['Montant du litige','Provision']));			
    }else{
		$("#evaluation_tableau").html("<p style='margin: auto;margin-top: 20%;'>Aucun résultat trouvé pour votre requête</br><b>Provision des affaires</b></p>")		
    }
		
		
	})
}

function getBillanTableau(ds,anne){
	ds.call('getSolution', [anne,"tableau"]).done(function(data) {
    if (data){
		$("#solution_tableau").html(getMyHTMLMulti(data,"Bilan financier",['Montant gagné','Montant perdu']));
    }else{
		$("#solution_tableau").html("<p style='margin: auto;margin-top: 20%;'>Aucun résultat trouvé pour votre requête</br><b>Bilan financier</b></p>")		
    }
	})
}

function getMyDataBare(data,nameOfGraph,typeOfGraph,modeGraph){
	myDataSplit = data.split('<>');
	data = [];
	myDataP = [];
	extension = "";
	if(modeGraph=='nombre'){extension = " Affaire(s)";}
	else if (["valeur","frais","honoraires"].indexOf(modeGraph) >= 0){extension = " MAD";}
	else if (["duree"].indexOf(modeGraph) >= 0){extension = " Jours";}
	for(var i=0;i<myDataSplit.length-1;i++){
		column = myDataSplit[i].split('=>');
		//alert(parseInt(column[1]))
		//alert(column[1])
		myDataP.push({  label: column[0], y: parseInt(column[1])});
	}
	data = [{
		name: nameOfGraph,
		toolTipContent: "{label} : <strong>{y}</strong>"+extension,
		type: typeOfGraph,
		indexLabelPlacement: "outside",
		//showInLegend: true,
		//legendText: 'okkkkkkkk',
		dataPoints:myDataP
		}]
	return data
}




function getMyHTML(data,titre){
	myDataSplit = data.split('<>');
	html = "<h1>"+titre+"</h1><table class='tableStatistique'><thead><tr><td class='headTDL'>Libellé</td><td class='headTDR'>Valeur</td></tr></thead>"
	for(var i=0;i<myDataSplit.length-1;i++){
		column = myDataSplit[i].split('=>');
		html+="<tr><td>"+column[0]+"</td><td style='text-align: center;'>"+column[1]+"</td></tr>"
	}
	html += "</table>"
	return html
}

function getMyHTMLMulti(data,titre,collum){
	myDataSplit = data.split('<>');
	html = "<h1>"+titre+"</h1><table class='tableStatistique' style='width: 70%;'><thead><tr><td class='headTDL'>Année</td><td style='text-align:center;'>"+collum[0]+"</td><td class='headTDR'>"+collum[1]+"</td></tr></thead>"
	for(var i=0;i<myDataSplit.length-1;i++){
		column = myDataSplit[i].split('=>');
		html+="<tr><td style='text-align:center;'>"+column[0]+"</td><td style='text-align: center;'>"+column[1]+"</td><td style='text-align: center;'>"+column[2]+"</td></tr>"
	}
	html += "</table>"
	return html
}



function prepartDataAndMenu(ds,divID,titre,typeField,typeGraph,modeGraph,year,avocat,categorie,statut,etat,nature,typePartie,cgiPartie){
	
	$(".oe_view_manager_header").remove();
	//$('#dataHTML').html("<div class='statistique_body'><div id='chartContainer'style='height:300px;width:100%;margin-top:20px;'></div></div>");
	ds.call('getSimpleCase', [typeField,modeGraph,year,avocat,categorie,statut,etat,nature,typeGraph,typePartie,cgiPartie]).done(function(data) {

    if (data){
		if (typeGraph=="tableau"){
			$("#"+divID).show();
			$("#"+divID).html(getMyHTML(data,titre))
		}else{
			$("."+divID).show();
			var chart = new CanvasJS.Chart(divID, {
				animationEnabled: true,
				exportFileName: titre,
				exportEnabled: true,
				title: {text: titre},
				data: getMyDataBare(data,titre,typeGraph,modeGraph)
			});
			chart.render();
			$('.canvasjs-chart-credit').css('display','none');
		}
    }else{
	
	$("#"+divID).html("<p style='margin: auto;margin-top: 20%;'>Aucun résultat trouvé pour votre requête</br><b>"+titre+"</b></p>");
	$("."+divID).hide();	
    }
	})
}





function prepartYearSelect(ds){
	resultat = ""
	ds.call('getYear', []).done(function(data) {
		resultat = "<option value='All' selected='true'>Tous</option>";
		if (data){
			myDataSplit = data.split('<>');
			for(var i=0;i<myDataSplit.length-1;i++){
				yearValue = myDataSplit[i].split('.');
				resultat += "<option value='"+yearValue[0]+"'>"+yearValue[0]+"</option>"
			}
		}
		return $("#yearValue").html(resultat);
	})
}







function prepartAvocatsSelect(ds){
	resultat = ""
	ds.call('getAvocats', []).done(function(data) {
		if (data){
			myDataSplit = data.split('<>');
			resultat = "<option value='All' selected='true'>Tous</option>";
			for(var i=0;i<myDataSplit.length-1;i++){
				avocats = myDataSplit[i].split('=>');
				resultat += "<option value='"+avocats[0]+"'>"+avocats[1]+"</option>"
			}
		}
		return $("#avocatValue").html(resultat);
	})
}

function prepartCategorieSelect(ds){
	resultat = ""
	ds.call('getCategorie', []).done(function(data) {
		if (data){
			myDataSplit = data.split('<>');
			resultat = "<option value='All' selected='true'>Tous</option>";
			for(var i=0;i<myDataSplit.length-1;i++){
				avocats = myDataSplit[i].split('=>');
				resultat += "<option value='"+avocats[0]+"'>"+avocats[1]+"</option>"
			}
		}
		return $("#categorieValue").html(resultat);
	})
}


function prepartNatureLitigeSelect(ds){
	resultat = ""
	ds.call('getNatureLitige', []).done(function(data) {
		if (data){
			myDataSplit = data.split('<>');
			resultat = "<option value='All' selected='true'>Tous</option>";
			for(var i=0;i<myDataSplit.length-1;i++){
				avocats = myDataSplit[i].split('=>');
				resultat += "<option value='"+avocats[0]+"'>"+avocats[1]+"</option>"
			}
		}
		return $("#natureValue").html(resultat);
	})
}


function prepartUsersInSelect(ds){
	resultat = ""
	ds.call('getAllUsers', []).done(function(data) {
		if (data){
			myDataSplit = data.split('<>');
			for(var i=0;i<myDataSplit.length-1;i++){
				avocats = myDataSplit[i].split('=>');
				resultat += "<option value='"+avocats[0]+"'>"+avocats[1]+"</option>"
			}
		}
		return $("#userAdd").html(resultat);
	})
}




function prepartSelectCretaire(ds,fields,idFields){
	resultat = ""
	ds.call('getValuesFromAffaire', [fields]).done(function(data) {
		if (data){
			myDataSplit = data.split('<>');
			resultat = "<option value='All' selected='true'>Tous</option>";
			for(var i=0;i<myDataSplit.length-1;i++){
				resultat += "<option value='"+myDataSplit[i]+"'>"+myDataSplit[i]+"</option>"
			}
		}
		return $(idFields).html(resultat);
	})
}




function getCalendar(ds,url,correspondance,event,facture){
    ds.call('getEcheances', [url,correspondance,event,facture]).done(function(data) {

            $('#calendarEcheances').fullCalendar({
                locale: 'fr',
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                firstDay: 1,
                columnFormat : 'dddd',
                monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
                monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
                dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
                dayNamesShort: ['Dim.','Lun.','Mar.','Mer.','Jeu.','Ven.','Sam.'],
                events: data,
                eventMouseout: function(data, event, view) {
                    $('#calendarEcheances div#PopUP'+data.id).qtip('api').reposition(event).destroy(event);
                },
                eventMouseover: function(data, event, view) {
                    if($('#calendarEcheances div#PopUP'+data.id).length==0){
                        $('#calendarEcheances').append('<div id="PopUP'+data.id+'"/>');
                    }
                    if($('#calendarEcheances div#PopUP'+data.id).length!=0){
                    var popup=$('#calendarEcheances div#PopUP'+data.id).qtip({
                        id: data.id,
                        prerender: true,
                        content: {
                            text: '',
                            title: ''
                        },
                        position: {
                            my: 'center top',
                            at: 'top center',
                            adjust: { x: 0, y: -50 }
                        },
                        hide: {event:'click mouseout unfocus', fixed: true, delay: 200,
                        effect: function() {$(this).hide('puff', 200);}}
                    }).qtip('api');
                    var content =popup.set({'content.title': data.title,'content.text': data.desc,'position.target':$(this).find('div')});
                    	content.reposition(event).show(event);
                    }
                },
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: '' //'month,agendaWeek,agendaDay'
                    }
            });
            $('.fc-button-today').html("Aujourd'hui");
        })
} 

String.prototype.decode = function(){
    return decodeURIComponent(escape(this));
}






