# -*- coding: utf-8 -*-

import math
import re
import time

from odoo import api, fields, models, tools, _
from odoo.exceptions import  ValidationError
import datetime
from datetime import date, datetime, timedelta
from datetime import datetime
import dateutil.relativedelta as relativedelta
from random import randint
import pytz

class notaire_frais_formalite(models.Model):
        _name = 'notaire.frais.formalite'

        _rec_name = "formalite_name"




        handle  = fields.Char('handle')
        frais_num  = fields.Integer('ID frais')
        formalite_id  = fields.Many2one('notaire.devis', 'Company')
        formalite_name  = fields.Char('Libellé')
        flag_hypo  = fields.Integer('flag_hypo')
        taux_formalite  = fields.Selection([('1','1%'),('1.5','1.5%'),('3','3%'),('4','4%'),('5','5%'),('6','6%'),('1.5+100','1.5% + 100'),('1.5+200','1.5% + 200'),('Fixe','Fixe'),('autre','Autre')], u"Taux")
        Total  = fields.Float('Total')




        @api.model
        def create(self, vals):
            return super(notaire_frais_formalite,self).create(vals)





        @api.model
        def write(self, vals):

            return super(notaire_frais_formalite,self).write(vals)




  



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
