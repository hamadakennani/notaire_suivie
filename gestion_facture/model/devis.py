# -*- coding: utf-8 -*-

import math
import re
import time

from odoo import api, fields, models, tools, _
from odoo.exceptions import  ValidationError
import datetime
from datetime import date, datetime, timedelta
from datetime import datetime
import dateutil.relativedelta as relativedelta
from random import randint
import pytz

class notaire_devis(models.Model):
        _name = 'notaire.devis'
        _inherit = ['mail.thread']
        _rec_name = "num_frais"






        @api.multi
        def send_mail_devis(self):
            self.ensure_one()
            template = self.env.ref('gestion_facture.mail_note_frais_modele', False)
            compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
            ctx = dict(
                default_model='notaire.devis',
                default_res_id=self.id,
                default_use_template=bool(template),
                default_template_id=template and template.id or False,
                default_composition_mode='comment',
                mark_invoice_as_sent=True,
                custom_layout="notaire_devis.mail_template_data_notification_email"
            )
            print "***********************"
            print ctx
            return {
                'name': _('Compose Email'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'views': [(compose_form.id, 'form')],
                'view_id': compose_form.id,
                'target': 'new',
                'context': ctx,
            }



        @api.onchange('montant_operation','nature_operation','taux_enregistrement','nombre_certificat')
        def onchange_frais_honoraire(self):
            total_frais_enregistrement = 0.0
            total_frais_cf = 0.0
            total_frais_cf_hypotheque = 0.0
            total_honoraire = 0.0 
            total_honoraire_tva = 0.0
            total_certificat = 0.0
            if self.nature_operation:
                if self.nature_operation in ["acquisition","promess_vente"]:
                        total_frais_enregistrement = (self.montant_operation * float(self.taux_enregistrement)) / 100
                        total_frais_cf = ((self.montant_operation * 1.5) / 100) + 100
                        total_honoraire  = (self.montant_operation * 1) / 100
                        total_honoraire_tva  = ((total_honoraire * 10) / 100)
                        total_certificat  = float(self.nombre_certificat) * 100
                        if self.montant_pret:
		                        hypotheque_1 = 0
		                        hypotheque_2 = 0
		                        hypotheque_3 = 0
		                        if self.montant_pret <= 250000:
			                        total_frais_cf_hypotheque = ((self.montant_pret * 0.5) / 100) +100
		                        elif self.montant_pret <= 5000000 and self.montant_pret > 250000:
			                        hypotheque_1 = ((250000 * 0.5) / 100)
			                        hypotheque_2 = ((self.montant_pret - 250000) * 1.5) / 100
			                        total_frais_cf_hypotheque = hypotheque_1 + hypotheque_2 + 100
		                        elif self.montant_pret > 5000000:
			                        hypotheque_1 = ((250000 * 0.5) / 100)
			                        hypotheque_2 = ((5000000) * 1.5) / 100
			                        hypotheque_3 = ((self.montant_pret - 5000000 ) * 0.5) / 100
			                        total_frais_cf_hypotheque = hypotheque_1 + hypotheque_2 + hypotheque_3 + 100
            if self.nature_operation:
                if self.nature_operation in ["promess_vente"]:
                        total_frais_enregistrement = 200
                        total_frais_cf = 0
                        total_honoraire  = 1200
                        total_honoraire_tva  = 120
                        total_certificat  = float(self.nombre_certificat) * 100
            if self.nature_operation:
                if self.nature_operation in ["constitution_hypotheque"]:
                        total_frais_enregistrement = 200
                        if self.montant_pret:
		                        hypotheque_1 = 0
		                        hypotheque_2 = 0
		                        hypotheque_3 = 0
		                        if self.montant_pret <= 250000:
			                        total_frais_cf_hypotheque = ((self.montant_pret * 0.5) / 100) +100
		                        elif self.montant_pret <= 5000000 and self.montant_pret > 250000:
			                        hypotheque_1 = ((250000 * 0.5) / 100)
			                        hypotheque_2 = ((self.montant_pret - 250000) * 1.5) / 100
			                        total_frais_cf_hypotheque = hypotheque_1 + hypotheque_2 + 100
		                        elif self.montant_pret > 5000000:
			                        hypotheque_1 = ((250000 * 0.5) / 100)
			                        hypotheque_2 = ((5000000) * 1.5) / 100
			                        hypotheque_3 = ((self.montant_pret - 5000000 ) * 0.5) / 100
			                        total_frais_cf_hypotheque = hypotheque_1 + hypotheque_2 + hypotheque_3 + 100
                        total_honoraire  = 1200
                        total_honoraire_tva  = 120
                        total_certificat  = float(self.nombre_certificat) * 100
            if self.nature_operation:
                if self.nature_operation in ["mainleve_hypotheque"]:
                        total_frais_enregistrement = 200
                        total_frais_cf = 500
                        total_honoraire  = 1200
                        total_honoraire_tva  = 120
                        total_certificat  = float(self.nombre_certificat) * 100
                        
            
            return {'value': {
                        'frais_enregistrement':total_frais_enregistrement,
                        'frais_cf': total_frais_cf,
                        'frais_honoraire': total_honoraire,
                        'frais_tva_honoraire': total_honoraire_tva,
                        'frais_certificat': total_certificat,
                        'frais_cf_hypotheque':total_frais_cf_hypotheque,
                        }}


        @api.onchange('frais_enregistrement','frais_cf','frais_honoraire','frais_tva_honoraire','frais_certificat','frais_cf_hypotheque','formalite')
        def onchange_total_frais(self):
            total_autre = 0
            total = 0
            if self.formalite:
                print "------------------------------------------------------"
                print self.formalite
            total = self.frais_enregistrement + self.frais_cf + self.frais_honoraire + self.frais_tva_honoraire + self.frais_certificat + self.frais_cf_hypotheque
                        
            
            return {'value': {
                        'montant_total':total,
                        }}

        #@api.model
        #def default_get(self,fields):
        #        res = super(notaire_devis, self).default_get(fields)
        #        formatmite = []
        #        formatmite = [(0, 0,{'formalite_name':'Enregistrement','Total':0.00})]
        #        formatmite.append((0, 0,{'formalite_name':'Conservation Foncière : Mutation','Total':0.00,}))
        #        formatmite.append((0, 0,{'formalite_name':'Conservation Foncière : Hypothèque','Total':0.00,}))
        #        formatmite.append((0, 0,{'formalite_name':'Certficat de propriété','Total':0.00,}))
        #        formatmite.append((0, 0,{'formalite_name':'Timbres, Minutes et Expédition','Total':0.00,}))
        #        formatmite.append((0, 0,{'formalite_name':'Frais du Dossier','Total':0.00,}))
        #        formatmite.append((0, 0,{'formalite_name':'Honoraires','Total':0.00,}))
        #        formatmite.append((0, 0,{'formalite_name':'T.V.A sur honoraires','Total':0.00,}))
        #        res["formalite"] = formatmite
        #        return res

        num_frais = fields.Char('N° note de frais',required=True)
        date_creation = fields.Date('Date de création',required=True)
        nature_operation = fields.Selection([('promess_vente','Promesse de vente'),('acquisition','Acquisition'),('donation','Donation'),('constitution_hypotheque','Constitution d\'hypothèque'),('mainleve_hypotheque','Mainlevée d\'hypothèque')], u"Nature d'opération")
        nom_client = fields.Char('Nom client')
        prenom_client = fields.Char('Prénom client')
        req_tf = fields.Selection([('Req','Req'),('TF','TF')] ,u"TF ou Réq",default="TF")
        avec_sans_avance = fields.Selection([('avec_avance','Avec avance'),('sans_avance','Sans Avance')], u"Avance")
        type_avance = fields.Selection([('avance_direct','Avance directement'),('avance_comptabilite','Avance par la comptabilité')] ,u"Mode de paiement")
        montant_operation = fields.Float('Montant',required=True)
        num_piece_identite = fields.Char('Numéro de piéce d\'identite')
        num_req = fields.Char("N° de la réquisition")
        num_tf = fields.Char("N° Titre foncier")
        montant_avance = fields.Float('Montant de l\'avance')
        formalite = fields.One2many("notaire.frais.formalite","formalite_id",u'Date de signature')
        montant_total = fields.Float('Montant total',readonly=True)
        nature_select = fields.Char('nature_select')
        afficher_print = fields.Integer('afficher_print')
        state = fields.Selection([('brouillon','Brouillon'),('annule','Annulée')], 'Statut', readonly=True,default="brouillon")
        codetypeidentifiant = fields.Selection([('CIN','CIN'),('PASS','Passeport'),('IF','Identifiant fiscal'),('ET_CV','Etat civil'),('3',u'Carte d\'immatriculation'),('CNSS','N° d\'affiliation à la CNSS') ,('PPR','Numero PPR') ,('Mineur','Mineur'),('CE','N° CARTE ETRANGERE')] ,u"Pièce d'identité")
        avec_sans_credit = fields.Selection([('avec_credit','Avec crédit bancaire'),('sans_credit','Sans crédit bancaire')], u"Crédit bancaire")
        acte_separe_tripartie = fields.Selection([(u'acte_separe',u'Acte séparé'),(u'acte_tripartite',u'Acte tripartite')], u"Type d'acte")
        hypotheque = fields.Selection([('seule_hypotheque','Une seule hypothèque'),('plus_hypotheque','Plusieurs hypothèques')] ,u"Hypothèque")
        montant_pret = fields.Float('Montant prêt')
        montant_pret_constitution = fields.Float('Montant prêt')
        client = fields.Many2one('res.partner',u"Client",required=True)
        company_id = fields.Many2one('res.company', 'Company')
        hypotheque_id = fields.One2many("note.hypotheque","hypotheque_id",u'Hypothèque')
        type_mainlevee = fields.Selection([('simple','Mainlevée simple'),('affaire','Mainlevée sur affaire')], "Mainlevée d'hypothèque")
        flag_acquisition_fond_commerce = fields.Integer('flag_acquisition_fond_commerce')
        flag_acquisition_part_sociele_action = fields.Integer('acquisition_part_sociele_action')
        type_acquisition_part_sociele_action = fields.Selection([('simple','Acquisition parts sociales / actions simple'),('immobiliere','Acquisition parts sociales / actions sociétés immobilières')], u"TF ou Réq")
        check_print = fields.Boolean('check print')
        taux_enregistrement = fields.Selection([('1','1%'),('1.5','1.5%'),('3','3%'),('4','4%'),('5','5%'),('6','6%'),('1000','1000'),('200','200'),('0','Exonéré')], u"Taux d'enregistrement")
        flag_create = fields.Boolean('flag_create')
        widget_save = fields.Char('widget_save')
        flag_hypo = fields.Integer('flag_hypo')
        frais_enregistrement = fields.Float("Frais d'enregistrement")
        frais_cf = fields.Float("Frais de la conservation foncière")
        frais_cf_hypotheque = fields.Float("Frais CF d\'hypothéque")
        frais_certificat = fields.Float("Frais Certificat de propriété")
        frais_honoraire = fields.Float("Honoraire")
        frais_droit_acte = fields.Float("Droit par acte",default=200)
        frais_tva_honoraire = fields.Float("TVA Honoraire")
        nombre_certificat = fields.Selection([('1','1'),('2','2'),('3','3'),('4','4'),('5','5'),('6','6'),('7','7'),('8','8'),('9','9'),('10','10')], u"Nombre des Certificat de propriété")




        @api.model
        def create(self, vals):
            print "--------------------------------------------------"
            print vals.get("num_frais")
            return super(notaire_devis,self).create(vals)









  



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
