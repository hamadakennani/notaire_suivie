# -*- coding: utf-8 -*-

import math
import re
import time

from odoo import api, fields, models, tools, _
from odoo.exceptions import  ValidationError
import datetime
from datetime import date, datetime, timedelta
from datetime import datetime
import dateutil.relativedelta as relativedelta
from random import randint
import pytz

class note_hypotheque(models.Model):
        _name = 'note.hypotheque'

        _rec_name = "montant_pret"

        @api.onchange('montant_pret','droit_hypotheque')
        def onchange_detail_ligne(self):
            print "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"
            droit_hypotheque = 0
            if self.montant_pret:
		            hypotheque_1 = 0
		            hypotheque_2 = 0
		            hypotheque_3 = 0
		            if self.montant_pret <= 250000:
			            total_hypotheque = ((self.montant_pret * 0.5) / 100) +100
		            elif self.montant_pret <= 5000000 and self.montant_pret > 250000:
			            hypotheque_1 = ((250000 * 0.5) / 100)
			            hypotheque_2 = ((self.montant_pret - 250000) * 1.5) / 100
			            total_hypotheque = hypotheque_1 + hypotheque_2 + 100
		            elif self.montant_pret > 5000000:
			            hypotheque_1 = ((250000 * 0.5) / 100)
			            hypotheque_2 = ((5000000) * 1.5) / 100
			            hypotheque_3 = ((self.montant_pret - 5000000 ) * 0.5) / 100
			            total_hypotheque = hypotheque_1 + hypotheque_2 + hypotheque_3 + 100
		            droit_hypotheque = total_hypotheque
            else:
		            droit_hypotheque = 0
            return {'value': {
                                "droit_hypotheque":droit_hypotheque
                            }}

        hypotheque_id = fields.Integer('hypotheque_id')
        montant_pret = fields.Float('Montant prêt')
        droit_hypotheque = fields.Float(u"Droits d'hypothèque")




        @api.model
        def create(self, vals):
            return super(note_hypotheque,self).create(vals)





        @api.model
        def write(self, vals):

            return super(note_hypotheque,self).write(vals)




  



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
