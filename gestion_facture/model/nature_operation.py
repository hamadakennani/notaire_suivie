# -*- coding: utf-8 -*-

import math
import re
import time

from odoo import api, fields, models, tools, _
from odoo.exceptions import  ValidationError
import datetime
from datetime import date, datetime, timedelta
from datetime import datetime
import dateutil.relativedelta as relativedelta
from random import randint
import pytz

class notaire_nature_operation(models.Model):
        _name = 'notaire.nature.operation'

        _rec_name = "nature_name"




        nature_id  = fields.Integer('ID Nature')
        nature_name  = fields.Char('Libellé')





        @api.model
        def create(self, vals):
            return super(notaire_nature_operation,self).create(vals)





        @api.model
        def write(self, vals):

            return super(notaire_nature_operation,self).write(vals)




  



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
