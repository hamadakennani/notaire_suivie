# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint


class suivie(models.Model):
    _name = 'notaire.suivie'
    _rec_name = 'num_dossier'



    num_dossier = fields.Char(string="Numéro du dossier")
    client = fields.Many2one('res.partner')
    dgi_montant_prevu = fields.Char(string="Montant prévu")
    dgi_date_or = fields.Date(string="Date obtention OR")
    dgi_date_paiement = fields.Date(string="Date du paiement")
    dgi_montant_paye = fields.Char(string="Montant payé")
    dgi_doc_or = fields.Binary(string="OR")
    ancfcc_montant_prevu = fields.Char(string="Montant prévu")
    ancfcc_date_depot = fields.Date(string="Date depôt en ligne")
    ancfcc_date_validation = fields.Date(string="Date validation")
    ancfcc_date_depot_physique = fields.Date(string="Date depôt physique")
    ancfcc_date_paiement = fields.Date(string="Date du paiement")
    ancfcc_montant_paye = fields.Char(string="Montant payé")
    ancfcc_date_rejet = fields.Date(string="Date rejet")
    ancfcc_motif = fields.Text(string="Motif")
    ancfcc_certificat = fields.Binary(string="Certificat")
    bnq_banque = fields.Selection([('cih', 'CIH')],'Banque',select=False)
    bnq_agence = fields.Char(string="Agence")
    bnq_contacte = fields.Char(string="Contacte")
    bnq_contacte_tele = fields.Char(string="Numéro du personne á contacté")
    bnq_date_notification = fields.Date(string="Date notification")
    bnq_date_engagement = fields.Date(string="Date d'envoi de l'engagement et acte")
    bnq_date_validation = fields.Date(string="Date validation")
    bnq_date_deblocage = fields.Date(string="Date déblocage")
    bnq_date_signature = fields.Date(string="Date signature")
    bnq_montant = fields.Float(string="Montant")
    tva_date_demande = fields.Date(string="Date demande TVA")
    tva_montant = fields.Char(string="Montant TVA")
    tva_date_validation = fields.Date(string="Date validation TVA")
    tva_date_virement_cdg = fields.Date(string="Date virement á la CDG")
    cdg_num_affaire = fields.Char(string="Numéro d'affaire")
    cdg_montant = fields.Char(string="Montant")
    cdg_type_payement = fields.Selection([('par_cheque', 'Chèque'),('par_virement', 'Virement bancaire'),('par_espece', 'Espèce')],'Mode de paiement',select=False)
    cdg_date_operation = fields.Date(string="Date demande TVA")
    cdg_liste_paiement = fields.One2many("notaire.cdg_liste_paiement","paiement",u'Liste des paiements')
    state = fields.Selection([('brouillon','Brouillon'),('annule','Annulée')], 'Statut', readonly=True,default="brouillon")
    nature_operation = fields.Selection([('promess_vente','Promesse de vente'),('acquisition','Acquisition'),('donation','Donation'),('constitution_hypotheque','Constitution d\'hypothèque'),('mainleve_hypotheque','Mainlevée d\'hypothèque')],u"Nature d'opérations")
    date_dossier = fields.Date(string="Date dossier")
    num_tf = fields.Char("Numéro TF")






class cdg_liste_paiement(models.Model):
    _name = 'notaire.cdg_liste_paiement'
    _rec_name = 'paiement'


    paiement = fields.Many2one('notaire.suivie')
    versement = fields.Selection([('first', '1er versement'),('seconde', 'Complément de versement')])
    type_payement = fields.Selection([('par_cheque', 'Chèque'),('par_virement', 'Virement bancaire'),('par_espece', 'Espèce')],'Mode de paiement',select=False)
    montant_recu = fields.Float('Montant de versement', required=True)
    date_operation = fields.Date('Date de l\'opération')




