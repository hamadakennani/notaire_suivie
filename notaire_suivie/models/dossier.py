# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint


class notaire_dossier(models.Model):
    _name = 'notaire.dossier'
    _rec_name = 'dossier_num'

    dossier_num = fields.Char(string="Numéro du dossier")
    dossier_numRep = fields.Char(string="Code")
    nature_operation = fields.Selection([('promess_vente','Promesse de vente'),('acquisition','Acquisition'),('donation','Donation'),('constitution_hypotheque','Constitution d\'hypothèque'),('mainleve_hypotheque','Mainlevée d\'hypothèque')],u"Nature d'opérations")
    client = fields.Many2one('res.partner', 'Client')
    tf_rq = fields.Selection([('Req','Req'),('TF','TF')], u"TF ou Réq",default="TF")
    avec_sans_avance = fields.Selection((('avec_avance','Avec avance'),('sans_avance','Sans Avance')), u"Avance")
    montant_avance = fields.Float('Montant de l\'avance')
    type_avance = fields.Selection((('avance_direct','Avance directement'),('avance_comptabilite','Avance par la comptabilité')), u"Mode de paiement")
    num_tf = fields.Char("Numéro TF")
    num_req = fields.Char("N° de la réquisition")
    dossier_date = fields.Date(u'Date du dossier')
    montant = fields.Float("Montant")
    type_hypotheque = fields.Selection((('seule_hypotheque','Une seule hypothèque'),('plus_hypotheque','Plusieurs hypothèques')), u"Hypothèque")
    hypotheque_ids = fields.One2many("notaire.hypotheque","hypotheque",u'Hypothèque')
    taux_enregistrement = fields.Selection([('1','1%'),('1.5','1.5%'),('3','3%'),('4','4%'),('5','5%'),('6','6%'),('1000','1000'),('200','200'),('0','Exonéré')], u"Taux d'enregistrement")
    type_acte= fields.Selection((('acte_separe','Acte séparé'),('acte_tripartite','Acte tripartite')), u"Type d'acte")
    credit_bancaire  = fields.Selection((('avec_credit','Avec crédit bancaire'),('sans_credit','Sans crédit bancaire')), u"Crédit bancaire")
    state = fields.Selection([('brouillon','Brouillon'),('annule','Annulée')], 'Statut', readonly=True)
    formalite = fields.One2many("notaire.frais.formalite","formalite_id",u'Date de signature')


    @api.model
    def default_get(self, fields):
        res =  super(notaire_dossier, self).default_get(fields)
        res["formalite"] = self.putFormalite()
        return res

    @api.onchange('nature_operation')
    def _onchange_field(self):
        print "oooooooooooooo"

        if self.nature_operation:
            self.formalite = self.putFormalite()

    def putFormalite(self):
        res =[]
        res.append((0, 0,{'formalite_name':'Enregistrement','Total':0.00}))
        res.append((0, 0,{'formalite_name':'Conservation Foncière : Mutation','Total':0.00,}))
        res.append((0, 0,{'formalite_name':'Conservation Foncière : Hypothèque','Total':0.00,}))
        res.append((0, 0,{'formalite_name':'Certficat de propriété','Total':0.00,}))
        res.append((0, 0,{'formalite_name':'Timbres, Minutes et Expédition','Total':0.00,}))
        res.append((0, 0,{'formalite_name':'Frais du Dossier','Total':0.00,}))
        res.append((0, 0,{'formalite_name':'Honoraires','Total':0.00,}))
        res.append((0, 0,{'formalite_name':'T.V.A sur honoraires','Total':0.00,}))
        return res





class hypotheque(models.Model):
    _name='notaire.hypotheque'

    hypotheque = fields.Many2one('notaire.dossier')
    montant_pret = fields.Float('Montant prêt')
    droit_hypotheque =fields.Float(u"Droits d'hypothèque")




class frais_formalite(models.Model):
    _name='notaire.frais.formalite'
    _rec_name='formalite_name'


    handle = fields.Char('handle')
    frais_num = fields.Integer('ID frais')
    formalite_id = fields.Many2one('notaire.dossier')
    formalite_name = fields.Char('Libellé')
    flag_hypo = fields.Integer('flag_hypo')
    taux_formalite = fields.Selection((('1','1%'),('1.5','1.5%'),('3','3%'),('4','4%'),('5','5%'),('6','6%'),('1.5+100','1.5% + 100'),('1.5+200','1.5% + 200'),('Fixe','Fixe'),('autre','Autre'),('advalorem','Ad-valorem')), u"Taux")
    Total = fields.Float('Total')
