{
    'name': 'Suivie des dossiers',
    'version': '1.0',
    'summary': 'Groupe H&M',
    'description': '',
    'category': 'Groupe H&M',
    'author': 'H&M',
    'website': '',
    'license': '',
    'depends': ['base','report','product'],
    'data': [
        'data/report_paperformat_data.xml',
        'views/suivie_view.xml',
        'static/xml/data.xml',
        'reports/vente_report.xml',
        'views/menu/menu_view.xml',
    ],
    'qweb': [

    ],
    'css': [],
    'js': [],
    'installable': True,
    'auto_install': False,
}
